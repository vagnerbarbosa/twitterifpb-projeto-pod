package listerners;

import antlr.StringUtils;
import br.edu.ifpb.entidades.Usuario;
import java.io.IOException;
import javax.faces.FacesException;
import javax.faces.FactoryFinder;
import javax.faces.application.NavigationHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.faces.render.RenderKit;
import javax.faces.render.RenderKitFactory;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpSession;
import org.glassfish.admin.rest.Constants;
import org.jboss.weld.context.unbound.RequestContextImpl;
import org.primefaces.context.RequestContext;

public class ControleAcessoListener implements PhaseListener {
 
    private static final long serialVersionUID = 1L;
 
    public ControleAcessoListener() {
    }
 
    @Override
    public void afterPhase(PhaseEvent event) {
 
     FacesContext facesContext = event.getFacesContext();
     String currentPage = facesContext.getViewRoot().getViewId();
 
     boolean isLoginPage = (currentPage.lastIndexOf("index.jsf") > -1);
     HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
     Object currentUser = session.getAttribute("currentUser");
 
     if (!isLoginPage && currentUser == null) {
       NavigationHandler nh = facesContext.getApplication().getNavigationHandler();
       nh.handleNavigation(facesContext, null, "index");
     } else if (isLoginPage && currentUser != null) {
            // Se o usuário logado tentar acessar a página de login ele é
            // redirecionado para a página inicial
            NavigationHandler nh = facesContext.getApplication().getNavigationHandler();
            nh.handleNavigation(facesContext, null, "home");
     }
   }
    
    @Override
    public void beforePhase(PhaseEvent event) {
    }
 
    @Override
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }
}

