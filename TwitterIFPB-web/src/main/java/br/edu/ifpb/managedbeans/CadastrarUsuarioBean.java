package br.edu.ifpb.managedbeans;

import br.edu.ifpb.entidades.Usuario;
import br.edu.ifpb.sessionbeans.interfaces.UsuariosSessionBeanLocal;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@RequestScoped
public class CadastrarUsuarioBean implements Serializable {

    @EJB
    private UsuariosSessionBeanLocal UsuariosManaged;
    public Usuario usuario = new Usuario();

    public CadastrarUsuarioBean() {
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public void efetuarCadastro() {
        UsuariosManaged.persistirUsuario(usuario);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("OK!"));
    }
}
