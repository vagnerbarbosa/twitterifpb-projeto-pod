package br.edu.ifpb.managedbeans;

import br.edu.ifpb.entidades.Mensagem;
import br.edu.ifpb.entidades.Usuario;
import br.edu.ifpb.sessionbeans.interfaces.MensagensSessionBeanLocal;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

@ManagedBean(name = "mensagemBean")
@SessionScoped
public class MensagemBean implements Serializable {
    
    public Mensagem mensagem = new Mensagem();
    public Usuario usuario = new Usuario();
    @EJB
    private MensagensSessionBeanLocal mensagensSessionBean;
    
    @ManagedProperty("#{usuarioBean}")
    private UsuarioBean usuarioBean;

    public MensagemBean() {
    }

    public Mensagem getMensagem() {
        return mensagem;
    }

    public void setMensagem(Mensagem mensagem) {
        this.mensagem = mensagem;
    }

    public MensagensSessionBeanLocal getMensagensSessionBean() {
        return mensagensSessionBean;
    }

    public void setMensagensSessionBean(MensagensSessionBeanLocal mensagensSessionBean) {
        this.mensagensSessionBean = mensagensSessionBean;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public UsuarioBean getUsuarioBean() {
        return usuarioBean;
    }

    public void setUsuarioBean(UsuarioBean usuarioBean) {
        this.usuarioBean = usuarioBean;
    }    
    
    
    public void postarMensagem() {
        usuario = usuarioBean.usuarioDaSessao();
        mensagem.setUsuario(usuario);
        System.out.println(mensagem.getUsuario().getLogin());
        mensagensSessionBean.persistirMensagem(mensagem);
    }
    
    
}
