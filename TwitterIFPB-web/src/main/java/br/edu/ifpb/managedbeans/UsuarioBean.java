package br.edu.ifpb.managedbeans;

import br.edu.ifpb.entidades.Usuario;
import br.edu.ifpb.sessionbeans.interfaces.UsuariosSessionBeanLocal;
import java.io.IOException;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean(name = "usuarioBean")
@SessionScoped
public class UsuarioBean implements Serializable {

    private Usuario usuario = new Usuario();
    @EJB
    private UsuariosSessionBeanLocal userSessionBean;

    public UsuarioBean() {
    }       

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public UsuariosSessionBeanLocal getUserSessionBean() {
        return userSessionBean;
    }

    public void setUserSessionBean(UsuariosSessionBeanLocal userSessionBean) {
        this.userSessionBean = userSessionBean;
    }   
    
    public String logarUsuario() throws IOException {
        String paginaRetorno = "index.jsf";
        System.out.println(usuario.getSenha());
        Usuario user = (Usuario) userSessionBean.autenticarUsuario(usuario.getSenha());
        System.out.println(user.getID());
        System.out.println(user.getEmail());
        System.out.println(user.getLogin());
        System.out.println(user.getSenha());
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("currentUser", user);
        if (user.getLogin().equals(usuario.getLogin())) {
            System.out.println("Logou!");                       
            paginaRetorno = "user/home.jsf";
            FacesContext.getCurrentInstance().getExternalContext().redirect(paginaRetorno);            
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Login ou senha inválidos"));
            System.out.println("Login ou senha inválidos");                        
            FacesContext.getCurrentInstance().getExternalContext().redirect(paginaRetorno);            
        }        
        return paginaRetorno;        
    }   

    public Usuario usuarioDaSessao() {
        Usuario usuarioDaSessao = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("currentUser");
        return usuarioDaSessao;
    }

    public void encerrarSessao() throws IOException {
        usuario = new Usuario();
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().clear();        
        FacesContext.getCurrentInstance().getExternalContext().redirect("../index.jsf");
    }
    
}
