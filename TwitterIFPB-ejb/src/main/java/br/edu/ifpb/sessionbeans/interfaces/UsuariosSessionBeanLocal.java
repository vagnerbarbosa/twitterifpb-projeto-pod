package br.edu.ifpb.sessionbeans.interfaces;

import br.edu.ifpb.entidades.Usuario;

public interface UsuariosSessionBeanLocal {
    
    public void persistirUsuario(Usuario u);
    public Usuario autenticarUsuario(String senha);
    
}
