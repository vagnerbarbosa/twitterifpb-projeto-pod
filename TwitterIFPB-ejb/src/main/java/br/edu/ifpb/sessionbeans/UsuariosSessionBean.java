package br.edu.ifpb.sessionbeans;

import br.edu.ifpb.entidades.Usuario;
import br.edu.ifpb.sessionbeans.interfaces.UsuariosSessionBeanLocal;
import java.io.Serializable;
import javax.ejb.Local;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateful
@Local(UsuariosSessionBeanLocal.class)
public class UsuariosSessionBean implements UsuariosSessionBeanLocal, Serializable {
  
    @PersistenceContext(unitName = "TwitterIFPB")
    private EntityManager manager;

    @Override
    public void persistirUsuario(Usuario u) {
        manager.persist(u);
    }

    @Override
    public Usuario autenticarUsuario(String senha) {
        Usuario usuario = new Usuario();
         try {
        Query query = manager.createNamedQuery("usuario.recuperar");
        query.setParameter("senha", senha);        
        usuario = (Usuario) query.getSingleResult();       
         } catch (Exception e) {
        }
        return usuario;
    }
}
