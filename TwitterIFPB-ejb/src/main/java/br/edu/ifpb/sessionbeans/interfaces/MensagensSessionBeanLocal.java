package br.edu.ifpb.sessionbeans.interfaces;

import br.edu.ifpb.entidades.Mensagem;

public interface MensagensSessionBeanLocal {

    public void persistirMensagem(Mensagem m);
}
