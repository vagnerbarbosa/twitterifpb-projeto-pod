package br.edu.ifpb.sessionbeans;

import br.edu.ifpb.entidades.Mensagem;
import br.edu.ifpb.sessionbeans.interfaces.MensagensSessionBeanLocal;
import java.io.Serializable;
import javax.ejb.Local;
import javax.ejb.Stateful;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateful
@Local(MensagensSessionBeanLocal.class)
public class MensagensSessionBean implements MensagensSessionBeanLocal, Serializable {

    @PersistenceContext(unitName = "TwitterIFPB")
    private EntityManager manager;    

    @Override
    public void persistirMensagem(Mensagem m) {
        manager.persist(m);
    }
    
}
